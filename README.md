<div align="center">
  <img src="https://codeberg.org/teaserbot-labs/delightful-small-ai/raw/branch/main/small-ai-logo.png">
</div>

# delightful small AI [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)

A curated list of humane artificial intelligence resources that are open and accessible.

## Contents

- [Candidates for this list](#candidates-for-this-list)
- [Organizations](#organizations)
- [Maintainers](#maintainers)
- [Contributors](#contributors)
- [License](#license)

## Candidates for this list

**Small AI** operates at 'human scale' and we prefer FOSS projects (apps, libraries, frameworks, db's) that align as close as possible to principles of [Small Technology](https://small-tech.org/about/#small-technology) as developed by [Laura Kalbag](https://laurakalbag.com) and [Aral Balkan](https://ar.al) at the [Small Technology Foundation](https://small-tech.org).

> _"Small Technology are everyday tools for everyday people designed to increase human welfare, not corporate profits."_

[![The Principles of Small Technology](https://codeberg.org/teaserbot-labs/delightful-small-ai/raw/branch/main/small-tech-10-principles.png)](https://small-tech.org/about/#small-technology)

_(image credit [small-tech.org](https://small-tech.org/about/#small-technology): principles of small technology)_

## FOSS Projects

| Name | Description | Language | License |
| :--- | :--- | :--- | :--- |
| [Grakn](https://grakn.ai/) | An intelligent database that provides the knowledge foundation for cognitive and intelligent (e.g. AI) systems. | Java | AGPL-3.0 |
| [MyCroft AI](https://mycroft.ai/) | The open and private voice assistant. | Python | Apache-2.0 |

## Organizations

| Name | Description |
| :--- | :--- |
| [European AI Fund](https://europeanaifund.org/) | **The European AI Fund** is a philanthropic initiative to shape the direction of AI in Europe. Our long-term vision is to promote an ecosystem of European public interest and civil society organisations working on policy and technology, based on a diversity of actors and a plurality of goals that represents society as a whole. |
| [Humane AI](https://www.humane-ai.eu/) | **Human-Centered Artificial Intelligence**: We are designing the principles for a new science that will make artificial intelligence based on European values and closer to Europeans. This new approach works toward AI systems that augment and empower all Humans by understanding us, our society and the world around us. |
| [Waag AI Culture Lab](https://waag.org/en/lab/ai-culture-lab) | **The AI Culture Lab** sets out to explore the role and potential of AI in our shared futures, understanding AI systems both as technological and cultural phenomena. It adds humanities, artistic and citizen perspectives to the already existing excellent data and computing research practices at the Amsterdam Science Park. |

## Maintainers

If you have questions or feedback regarding this list, then please create an [Issue](https://codeberg.org/teaserbot-labs/delightful-gamification/issues) in our tracker, and optionally `@mention` one or more of our maintainers:

- [`@circlebuilder`](https://codeberg.org/circlebuilder)

## Contributors

With delight we present you some of our [delightful contributors](delightful-contributors.md) (please [add yourself](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors) if you are missing).

## License

[![CC0 Public domain. This work is free of known copyright restrictions.](https://i.creativecommons.org/p/mark/1.0/88x31.png)](LICENSE)

